const express = require('express');
const path = require('path');
const uuid = require('uuid');
//const htmlFile = require('./public/htmlFile.html')

const app = express();

app.get('/html', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'htmlFile.html'))
});

app.get('/json', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'jsonString.json'))
});

app.get('/uuid/', (req, res) => {
    res.send(`{
        "uuid": ${uuid.v4()}
    }`)
});

app.get('/status/:code', (req, res) => {
    res.send(`You are sending the request for the Status Code: ${req.params.code}`)
});

app.get('/delay/:seconds', (req, res) => {
    setTimeout(() => {
        res.send(`Request is successful after ${req.params.seconds} seconds delay.`)
    }, req.params.seconds * 1000)
});



const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));


